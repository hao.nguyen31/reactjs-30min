import React from 'react'
import Todo from './Todo'

export function TodoList({todoList, onCheckBtnClick}) {
  return (
    <>
      {
        // console.log(onCheckBtnClick)
        todoList.map(task => <Todo key={task.id} task={task} onCheckBtnClick={onCheckBtnClick}/>)
      }
    </>
  )
}
