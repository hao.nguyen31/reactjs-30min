import { TodoList } from "./components/TodoList";
import Textfield from '@atlaskit/textfield'
import Button from "@atlaskit/button";
import { useCallback, useState } from "react";
import { v4 } from "uuid";

function App() {
  const [todoList, setTodoList] = useState([]);
  const [textInput, setTextInput] = useState('');

  const onTextInputChange = useCallback((ev) => {
      setTextInput(ev.target.value);
    }, []);

  const onAddBtnClick = useCallback((ev) => {
    // console.log('click add btn')
    setTodoList([{id: v4(), name: textInput, isCompleted: false},...todoList])
    // console.log(`todoList: ${todoList}`)
    setTextInput('');
  }, [todoList, textInput]);


  const onCheckBtnClick = useCallback((id) => {
    setTodoList(prevState => 
      prevState.map(task => task.id === id ? {...task, isCompleted: true} : task))
  }, []);

  return (
    <>
      <h3>Danh sách cần làm</h3>
      <Textfield
        name='add-todo'
        placeholder='Thêm việc cần làm...'
        elemAfterInput={
          <Button isDisabled={!textInput} appearance='primary' onClick={onAddBtnClick}>
            Thêm
          </Button>
        } 
        style={{ padding: "10px 10px 10px" }}
        value={textInput}
        onChange={onTextInputChange}
        ></Textfield>

        <TodoList todoList={todoList} onCheckBtnClick={onCheckBtnClick}/>
    </>
  );
}

export default App;